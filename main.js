/**
 * Created by sakuyasugo on 2015/11/19.
 */

var App = {};

(function (App) {
  "use strict";

  App.class = App.class || {};

  var ATTR_CLASS_HOVER = "hover";

  var DragDrop = function (element) {
    this.render = new FileReader();
    this.element = element;
    this.setEvents();
  };

  DragDrop.prototype = {
    onload: function () {},

    nop: function (event) {
      event.preventDefault();
    },

    success: function (func) {
      this.onload = func;
      return this;
    },

    drop: function (dt) {
      var self = this;
      var file = dt.files[0];

      this.render.onload = function (event) {
        self.onload(event.target.result);
      };

      this.render.readAsDataURL(file);
    },

    setEvents: function () {
      var self = this;

      this.element.addEventListener("drop", function (event) {
        event.preventDefault();
        self.drop(event.dataTransfer);
      }, false);

      this.element.addEventListener("dragenter", function (event) {
        self.element.setAttribute("class", ATTR_CLASS_HOVER);
      }, false);

      this.element.addEventListener("dragleave", function (event) {
        self.element.removeAttribute("class", ATTR_CLASS_HOVER);
      }, false);

      this.element.addEventListener("dragover", this.nop, false);
    }
  };

  App.class.DragDrop = DragDrop;
})(App);


(function (App) {
  "use strict";

  var dd = new App.class.DragDrop(document.getElementById("dd"));
  var img = document.getElementById("img");

  dd.success(function (dataURI) {
    var _img = document.createElement("img");
    _img.src = dataURI;
    img.appendChild(_img);
  });

})(App);
